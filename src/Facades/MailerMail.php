<?php
/**
 * Created by PhpStorm.
 * User: aitspeko
 * Date: 08/08/2017
 * Time: 08:55
 */

namespace Lshtmweb\MailerLaravel\Facades;


use Illuminate\Support\Facades\Facade;

class MailerMail extends Facade
{
        protected static function getFacadeAccessor()
        {
                return 'mailer-mail';
        }
}