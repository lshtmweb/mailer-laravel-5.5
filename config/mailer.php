<?php
/**
 * Created by PhpStorm.
 * User: aitspeko
 * Date: 08/08/2017
 * Time: 08:48
 */

return [
    'url' => env('MAILER_URL', 'http://mailer.app'),
    'id'  => env('MAILER_APP_ID'),
    'key' => env('MAILER_APP_KEY')
];