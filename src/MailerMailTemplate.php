<?php
/**
 * Created by PhpStorm.
 * User: aitspeko
 * Date: 08/08/2017
 * Time: 08:44
 */

namespace Lshtmweb\MailerLaravel;


use Illuminate\Database\Eloquent\Model;

/**
 * Class MailerMailTemplate
 *
 * @property int    id
 * @property string key
 * @property string extra_data
 * @property string template_id
 * @property string template_key
 * @property string description
 *
 * @package Lshtmweb\MailerLaravel
 */
class MailerMailTemplate extends Model
{
        protected $table = 'mailer_templates';
}