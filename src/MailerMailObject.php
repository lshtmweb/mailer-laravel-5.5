<?php
/**
 * Created by PhpStorm.
 * User: aitspeko
 * Date: 20/07/2017
 * Time: 16:46
 */

namespace Lshtmweb\MailerLaravel;


use Illuminate\Support\Collection;

/**
 * @property Collection attachments
 */
class MailerMailObject
{
        use SendsMailerEmails;

        public function __construct()
        {
                $this->attachments = new Collection;
        }
}