<?php
/**
 * Created by PhpStorm.
 * User: aitspeko
 * Date: 07/08/2017
 * Time: 16:42
 */

namespace Lshtmweb\MailerLaravel;


use Throwable;

class MailerMailException extends \Exception
{
        public function __construct($message = "General Mailer Exception has occurred", $code = 0, Throwable $previous = null)
        {
                parent::__construct($message, $code, $previous);
        }
}